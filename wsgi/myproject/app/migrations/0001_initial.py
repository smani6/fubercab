# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cab',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.PositiveIntegerField(unique=True, verbose_name='members')),
                ('cab_type', models.CharField(default=b'FuberMini', max_length=30, verbose_name='Cab Type', choices=[(b'FuberMini', b'FuberMini'), (b'FuberMicro', b'FuberMicro'), (b'FuberPrime', b'FuberPrime')])),
                ('color', models.CharField(default=b'black', max_length=10, verbose_name='Cab color', choices=[(b'black', b'black'), (b'white', b'white'), (b'pink', b'pink')])),
                ('latitude', models.FloatField(verbose_name='Cab Latitude Position')),
                ('longitube', models.FloatField(verbose_name='Cab Longitude Position')),
                ('is_assigned', models.BooleanField(default=False)),
                ('driver_name', models.CharField(max_length=250, verbose_name='Cab Drive Name')),
                ('created_datetime', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'db_table': 'cab',
            },
        ),
        migrations.CreateModel(
            name='CabBooking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('fare', models.PositiveSmallIntegerField(default=0, verbose_name='Cab Fare')),
                ('start_time', models.DateTimeField()),
                ('end_time', models.DateTimeField(null=True, blank=True)),
                ('start_latitude', models.FloatField()),
                ('start_longitude', models.FloatField()),
                ('end_latitude', models.FloatField(null=True, blank=True)),
                ('end_longitude', models.FloatField(null=True, blank=True)),
                ('is_running', models.BooleanField(default=True)),
                ('cab', models.ForeignKey(to='app.Cab')),
            ],
            options={
                'db_table': 'cab_booking',
            },
        ),
    ]
