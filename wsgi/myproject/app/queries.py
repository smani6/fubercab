cab_selection_query = """
                SELECT id, number, cab_type, color, is_assigned, latitude, longitude,
                SQRT(
                POW(69.1 * (latitude - %s), 2) +
                POW(69.1 * (%s - longitude) * COS(latitude / 57.3), 2)) AS distance
                FROM cab  WHERE is_assigned = 0
            """
