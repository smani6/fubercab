from rest_framework import serializers
from .models import Cab, CabBooking


class StartTripReqSerializer(serializers.Serializer):

    latitude = serializers.FloatField(required=True)
    longitude = serializers.FloatField(required=True)
    color = serializers.CharField(required=False,allow_blank=True)


class EndTripReqSerializer(serializers.Serializer):

    latitude = serializers.FloatField(required=True)
    longitude = serializers.FloatField(required=True)


class CabSerializer(serializers.ModelSerializer):

    class Meta:
        model = Cab


class CabBookingSerializer(serializers.ModelSerializer):

    cab = CabSerializer()

    class Meta:
        model = CabBooking
