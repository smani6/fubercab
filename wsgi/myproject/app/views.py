from django.shortcuts import render
from django.views.generic import View
from django.http import HttpResponse
from .serializer import StartTripReqSerializer, EndTripReqSerializer, CabSerializer, CabBookingSerializer
from .queries import cab_selection_query
from .models import Cab, CabBooking
from rest_framework import generics
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework import status
from django.views.generic import (
    ListView, DetailView,
    UpdateView, CreateView, DeleteView)
import logging
log = logging.getLogger(__name__)


class StartTripApi(generics.GenericAPIView):

    @csrf_exempt
    def post(self, request):
        try:
            log.info("Entering Start trip API Post method ")
            print request
            data = request.data.copy()
            print data
            serializer = StartTripReqSerializer(data=data)
            if serializer.is_valid():
                latitude = serializer.validated_data['latitude']
                longitude = serializer.validated_data['longitude']
                color = serializer.validated_data.get("color")  

                #queryset = Cab.objects.filter(is_assigned=False, )

                query = cab_selection_query
                sql_color = ""
                if color:
                    sql_color = " AND color='%s' " % color

                query = query + sql_color + "ORDER BY distance LIMIT 1;"
                query = query % (latitude, longitude)
                
                available_cabs = Cab.objects.raw(query)
                available_cabs = list(available_cabs)
                print available_cabs
                try:
                    cab = available_cabs[0]
                except IndexError as e:
                    cab = None

                if not cab:
                    response = {"message": "No cabs available"}

                else:
                    start_cab_obj = cab.start_trip()
                    response = {"message": CabBookingSerializer(instance=start_cab_obj).data}

                return Response(response)

            else:
                print serializer.errors
                return Response(
                    serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            print e


class EndTrip(generics.GenericAPIView):

    queryset = CabBooking.objects.filter(is_running=True)

    def post(self, request, pk):

        data = request.data.copy()
        serializer = EndTripReqSerializer(data=data)

        if serializer.is_valid():

            latitude = serializer.validated_data["latitude"]
            longitude = serializer.validated_data["longitude"]
            obj = self.get_object()
            obj.end_trip(latitude, longitude)
            return Response({"message": CabBookingSerializer(instance=obj).data})

        return Response(
            serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CabListView(generics.ListAPIView):

    queryset = Cab.objects.filter(is_assigned=False)
    serializer_class = CabSerializer


class BookingHistoryView(generics.ListAPIView):

    queryset = CabBooking.objects.all()
    serializer_class = CabBookingSerializer


class HomeView(ListView):

    model = Cab
    template_name = 'home.html'

    def get_queryset(self):
        return Cab.objects.filter(is_assigned=False)


class BookCab(generics.GenericAPIView):

    def get(self, request):
        #cab_details = Cab.objects.filter(id=pk)
        # print cab_details[0]
        return render(request, 'book_cab.html')

    @csrf_exempt
    def post(self, request):
        try:
            print request.data.copy()
            latitude = request.data.get('latitude')
            print latitude
            longitude = request.data.get('longitude')
            color = request.data.get('color')
            req_dict = {'latitude': latitude, 'longitude': longitude, 'color': color}
            print req_dict
            return StartTripApi.as_view()(request)

        except Exception as e:
            print e
