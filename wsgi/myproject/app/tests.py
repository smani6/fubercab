from django.test import TestCase
from rest_framework.test import APIRequestFactory
from rest_framework.test import APITestCase
from rest_framework.test import APIClient
from app.models import Cab

# Create your tests here.

from django.core.urlresolvers import reverse
from rest_framework import status


class AppTest(APITestCase):

    def test_start_trip_1(self):

        url = reverse('start_trip')
        data = {"latitude": 12.9399438, "longitude": 77.626416}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.data, {"message": "No cabs available"})

    def test_add_cabs_1(self):

        Cab.objects.create(
            number=10000,
            cab_type="FuberMini",
            color='pink',
            latitude=90,
            longitude=140,
            is_assigned=False,
            driver_name="driver1")

        cab_details = Cab.objects.get(number=10000)
        self.assertEqual(cab_details.color, "pink")

    def test_start_trip_with_cab(self):

        Cab.objects.create(
            number=10000,
            cab_type="FuberMini",
            color='pink',
            latitude=90,
            longitude=140,
            is_assigned=False,
            driver_name="driver1")

        url = reverse('start_trip')
        data = {"latitude": 12.9399438, "longitude": 77.626416}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.data['message']['cab']['latitude'], 90)

    def test_cab_list(self):

        Cab.objects.create(
            number=10000,
            cab_type="FuberMini",
            color='pink',
            latitude=90,
            longitude=140,
            is_assigned=False,
            driver_name="driver1")

        url = reverse('cabs_details')
        response = self.client.get(url)
        self.assertEqual(response.data[0]['number'], 10000)

    def test_end_trip(self):

        Cab.objects.create(
            number=10000,
            cab_type="FuberMini",
            color='pink',
            latitude=90,
            longitude=140,
            is_assigned=False,
            driver_name="driver1")

        url = reverse('start_trip')
        data = {"latitude": 12.9399438, "longitude": 77.626416}
        response = self.client.post(url, data, format='json')

        trip_id = response.data['message']['id']

        data = {"latitude": 20, "longitude": 40}
        url = '/end/trip/%s' % trip_id

        end_trip_response = self.client.post(url, data, format='json')

        self.assertEqual(end_trip_response.data['message']['cab']['cab_type'], "FuberMini")

    def test_booking_history(self):

        Cab.objects.create(
            number=10000,
            cab_type="FuberMini",
            color='pink',
            latitude=90,
            longitude=140,
            is_assigned=False,
            driver_name="driver1")

        url = reverse('start_trip')
        data = {"latitude": 12.9399438, "longitude": 77.626416}
        response = self.client.post(url, data, format='json')
        trip_id = response.data['message']['id']

        url = '/end/trip/%s' % trip_id
        data = {"latitude": 20, "longitude": 40}
        end_trip_response = self.client.post(url, data, format='json')

        url = reverse('booking_history')
        booking_history_response = self.client.post(url, data, format='json')

        self.assertEqual(end_trip_response.data['message']['cab']['cab_type'], "FuberMini")
