from django.db import models
from datetime import datetime
from django.utils.translation import ugettext_lazy as _
import math
from django.utils import timezone
from django.core.validators import MaxValueValidator, MinValueValidator
# Create your models here.


class Cab(models.Model):

    FUBERMINI = "FuberMini"
    FUBERMICRO = "FuberMicro"
    FUBERPRIME = "FuberPrime"
    CAR_CHOICES = (
        (FUBERMINI, "FuberMini"),
        (FUBERMICRO, "FuberMicro"),
        (FUBERPRIME, "FuberPrime"),
    )

    BLACK = "black"
    WHITE = "white"
    PINK = "pink"
    COLOR_CHOICES = (
        (BLACK, 'black'),
        (WHITE, 'white'),
        (PINK, 'pink'),
    )

    number = models.PositiveIntegerField(verbose_name=_('cab number'), unique=True)
    cab_type = models.CharField(
        _("Cab Type"),
        max_length=30,
        choices=CAR_CHOICES,
        default=FUBERMINI)

    color = models.CharField(_("Cab color"), max_length=10, choices=COLOR_CHOICES, default=BLACK)
    latitude = models.FloatField(
        _("Cab Latitude Position"),
        validators=[
            MinValueValidator(-90),
            MaxValueValidator(+90)])
    longitude = models.FloatField(
        _("Cab Longitude Position"),
        validators=[
            MinValueValidator(-180),
            MaxValueValidator(+180)])
    is_assigned = models.BooleanField(default=False)
    driver_name = models.CharField(_("Cab Drive Name"), max_length=250)
    created_datetime = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "cab"

    def __unicode__(self):
        return "%s - %s" % (self.number, self.cab_type)

    def is_pink_color(self):

        if self.color == self.PINK:
            return True
        return False

    def start_trip(self):
        obj = CabBooking.objects.create(
            cab=self,
            start_latitude=self.latitude,
            start_longitude=self.longitude,
            start_time=timezone.now())
        self.is_assigned = True
        self.save()

        return obj


class CabBooking(models.Model):

    CHARGE_PER_MINUTE = 1
    CHARGE_PER_KILOMETER = 2
    PINK_CAB_CHARGE = 5
    BASE_FARE = 0
    cab = models.ForeignKey(Cab)
    fare = models.PositiveSmallIntegerField(_("Cab Fare"), default=BASE_FARE)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField(null=True, blank=True)
    start_latitude = models.FloatField()
    start_longitude = models.FloatField()
    end_latitude = models.FloatField(null=True, blank=True, validators=[
        MinValueValidator(-90),
        MaxValueValidator(+90)])
    end_longitude = models.FloatField(null=True, blank=True, validators=[
        MinValueValidator(-180),
        MaxValueValidator(+180)])
    is_running = models.BooleanField(default=True)

    class Meta:
        db_table = "cab_booking"

    def __unicode__(self):
        return "CabNo:%s DriverName:%s Fare:%s" % (self.cab.number, self.cab.driver_name, self.fare)

    def end_trip(self, latitude, longitude):

        self.end_time = datetime.now()
        self.end_latitude = latitude
        self.end_longitude = longitude
        self.save()
        self.calculate_fare()
        cab = self.cab
        cab.is_assigned = False
        cab.latitude = latitude
        cab.longitude = longitude
        cab.save()

    def calculate_fare(self):

        if self.is_running and self.fare == 0:

            total_distance = self.calculate_distance()
            distance_fare = total_distance * self.CHARGE_PER_KILOMETER

            total_minutes = self.calculate_minutes()
            minutes_fare = total_minutes * self.CHARGE_PER_MINUTE

            total_fare = distance_fare + minutes_fare

            if self.cab.is_pink_color():
                total_fare += self.PINK_CAB_CHARGE

            self.fare = total_fare
            self.is_running = False
            self.save()

    def calculate_distance(self):

        lat1, lon1 = self.start_latitude, self.start_longitude
        lat2, lon2 = self.end_latitude, self.end_longitude
        radius = 6371  # km

        dlat = math.radians(lat2 - lat1)
        dlon = math.radians(lon2 - lon1)
        a = math.sin(dlat / 2) * math.sin(dlat / 2) + math.cos(math.radians(lat1)) \
            * math.cos(math.radians(lat2)) * math.sin(dlon / 2) * math.sin(dlon / 2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
        d = radius * c

        return d

    def calculate_minutes(self):
        self.start_time = self.start_time.replace(tzinfo=None)
        print self.end_time
        print self.start_time
        trip_time = self.end_time - self.start_time
        minutes, seconds = divmod(trip_time.days * 86400 + trip_time.seconds, 60)
        return minutes
