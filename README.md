FuberApp: Cab Booking Service

Hosted at Openshift : http://fubercab-githubusersdata.rhcloud.com/

Services are based on REST API and trip details can be posted or retrieved.

Home Page of site - http://fubercab-githubusersdata.rhcloud.com/ shows the available cabs and by clicking on Book Cab - you can book the cab. 

To Show Cabs List:
    http://fubercab-githubusersdata.rhcloud.com/cabs 

To Show Booking History:
    http://fubercab-githubusersdata.rhcloud.com/booking/history 
    
To Add Cabs :

Go to admin page - http://fubercab-githubusersdata.rhcloud.com/admin/login/?next=/admin/
  Username : admin
  Password: password

Once logged in - click on the cabs app. You can see the existing cabs. 
Click Add cab to add cab details and click save.

Curl Commands for Rest based API:

    Start Trip:
        Request:
            curl -H "Content-Type: application/json" -X POST -d '{"latitude":12.9399438, "longitude":77.626416}' http://fubercab-githubusersdata.rhcloud.com/start/trip/
        
    Response:
    {
    "message": {
        "id": 3,
        "cab": {
            "id": 1,
            "number": 1000,
            "cab_type": "FuberMini",
            "color": "black",
            "latitude": 90,
            "longitude": 90,
            "is_assigned": true,
            "driver_name": "driver1",
            "created_datetime": "2016-08-07T14:24:30Z"
        },
        "fare": 0,
        "start_time": "2016-08-07T16:40:03.728486Z",
        "end_time": null,
        "start_latitude": 90,
        "start_longitude": 90,
        "end_latitude": null,
        "end_longitude": null,
        "is_running": true
       }
    }
          
          
    End Trip:
        Request:
            url : http://fubercab-githubusersdata.rhcloud.com/end/trip/<int:trip-id>
            curl -H "Content-Type: application/json" -X POST -d '{"latitude":12.9399438, "longitude":77.626416}' http://fubercab-githubusersdata.rhcloud.com/end/trip/1
        
        Response:
        {
        "message": {
        "id": 2,
        "cab": {
            "id": 2,
            "number": 1001,
            "cab_type": "FuberMicro",
            "color": "white",
            "latitude": 12.9399438,
            "longitude": 77.626416,
            "is_assigned": false,
            "driver_name": "driver2",
            "created_datetime": "2016-08-07T14:29:31Z"
        },
        "fare": 22842,
        "start_time": "2016-08-07T15:50:28",
        "end_time": "2016-08-07T16:43:43.656876",
        "start_latitude": -89,
        "start_longitude": 140,
        "end_latitude": 12.9399438,
        "end_longitude": 77.626416,
        "is_running": false
    }
    }
    
    
    Get Available Cabs:
        Request:
            curl -X GET -H "Content-Type: application/json" "http://fubercab-githubusersdata.rhcloud.com/cabs"
            
        Response:
        [
            {
            "id": 2,
            "number": 1001,
            "cab_type": "FuberMicro",
            "color": "white",
            "latitude": 12.9399438,
            "longitude": 77.626416,
            "is_assigned": false,
            "driver_name": "driver2",
            "created_datetime": "2016-08-07T14:29:31Z"
            },
            {
            "id": 3,
            "number": 1002,
            "cab_type": "FuberPrime",
            "color": "pink",
            "latitude": 90.9399438,
            "longitude": 97.626416,
            "is_assigned": false,
            "driver_name": "driver3",
            "created_datetime": "2016-08-07T14:30:04Z"
            }
        ]
        
     Get Booking History:
         
         Request:
             curl -X GET -H "Content-Type: application/json" "http://fubercab-githubusersdata.rhcloud.com/booking/history"
             
        Response:
         [
            {
            "id": 1,
            "cab": {
            "id": 3,
            "number": 1002,
            "cab_type": "FuberPrime",
            "color": "pink",
            "latitude": 90.9399438,
            "longitude": 97.626416,
            "is_assigned": false,
            "driver_name": "driver3",
            "created_datetime": "2016-08-07T14:30:04Z"
            },
            "fare": 14689,
            "start_time": "2016-08-07T14:32:01Z",
            "end_time": "2016-08-07T14:33:06Z",
            "start_latitude": 24.098979,
            "start_longitude": -179.901919,
            "end_latitude": 90.9399438,
            "end_longitude": 97.626416,
            "is_running": false
            },
            {
            "id": 2,
            "cab": {
            "id": 2,
            "number": 1001,
            "cab_type": "FuberMicro",
            "color": "white",
            "latitude": 12.9399438,
            "longitude": 77.626416,
            "is_assigned": false,
            "driver_name": "driver2",
            "created_datetime": "2016-08-07T14:29:31Z"
            },
            "fare": 22842,
            "start_time": "2016-08-07T15:50:28Z",
            "end_time": "2016-08-07T16:43:43Z",
            "start_latitude": -89,
            "start_longitude": 140,
            "end_latitude": 12.9399438,
            "end_longitude": 77.626416,
            "is_running": false
            }
         ]   
             
     